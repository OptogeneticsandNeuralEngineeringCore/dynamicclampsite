## A site to host the DC

The dynamic clamp should be a standard part of every cellular electrophysiologist’s toolbox. That it is not, even 25 years after its introduction, comes down to three issues: money, the disruption that adding dynamic clamp to an existing electrophysiology rig entails, and the technical prowess required of experimenters. These have been valid and limiting issues in the past, but no longer. Technological advances associated with the so-called “maker movement” render them moot. We demonstrate this by implementing a fast (~100 kHz) dynamic clamp system using an inexpensive microcontroller (Teensy 3.6). The overall cost of the system is less than USD$100, and assembling it requires no prior electronics experience. Modifying it – for example, to add Hodgkin-Huxley-style conductances – requires no prior programming experience. The system works together with existing electrophysiology data acquisition systems (for Macintosh, Windows, and Linux); it does not attempt to supplant them. Moreover, the process of assembling, modifying, and using the system constitutes a useful pedagogical exercise for students and researchers with no background but an interest in electronics and programming. We demonstrate the system’s utility by implementing conductances as fast as a transient sodium conductance and as complex as the Ornstein-Uhlenbeck conductances of the “point conductance” model of synaptic background activity.

 [1]: http://phlow.github.io/feeling-responsive/documentation/
 [2]: https://github.com/Phlow/feeling-responsive/blob/gh-pages/LICENSE
 [3]: http://phlow.github.io/feeling-responsive/info/
 [4]: https://www.youtube.com/watch?v=rLS-BEvlEyY
 [5]: https://github.com/TWiStErRob
 [6]: https://phlow.github.io/feeling-responsive/changelog/
 [7]: http://phlow.github.io/feeling-responsive/
 [8]: http://phlow.github.io/simplicity/
 [9]: #
 [10]: #
