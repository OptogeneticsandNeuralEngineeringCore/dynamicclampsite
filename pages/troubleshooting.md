---
layout: page-fullwidth
title: "Troubleshooting"
#meta_title: "Feeling Responsive Theme Changelog"
#subheadline: "Feeling Responsive Theme Changelog"
#teaser: "History and changelog of Feeling Responsive Theme"
#header:
#   image_fullwidth: "header_unsplash_9.jpg"
permalink: "/troubleshooting/"
---
Here are some troubleshooting tips in case you run into problems constructing or using the dynamic clamp system.

## TROUBLESHOOTING TIP 1

Make sure that every element (resistor, capacitor, wire, IC) that you put on the breadboard fits snugly and makes good electrical contact.

This point cannot be overemphasized: nine times out of ten when someone from our lab has had trouble, the cause has been a loose connection. This was especially a problem in our first attempts because we were using Sparkfun’s resistor kit to provide the system’s resistors. Sparkfun is a terrific company with lots of great products, but the resistor kit it chooses to stock is not among them — the wires are too thin to fit snugly on a breadboard. That is why we now recommend that people buy the Elegoo  resistor kit (see Parts menu); those resistors have thicker wire and fit well.

For the same reason, whenever you want to connect different points on the breadboard with a wire, we recommend you use solid 22AWG wire (e.g., [https://www.sparkfun.com/products/11367](https://www.sparkfun.com/products/11367)). Do not use jumper wire terminating in little pins (e.g., [https://www.sparkfun.com/products/10897](https://www.sparkfun.com/products/10897)). Even though these wires look nice and convenient, they are not good for breadboarding: the pins don’t fit snugly into the breadboard holes. They’re so loose that just brushing your finger against the wire might spoil the electrical contact. Stick to solid 22AWG wire — either insulated hookup wire ([https://www.sparkfun.com/products/11367](https://www.sparkfun.com/products/11367)) or a jumper wire kit that uses solid 22AWG wire ([https://www.sparkfun.com/products/124](https://www.sparkfun.com/products/124)).

## TROUBLESHOOTING TIP 2

Make sure that 0 V is really 0 V.

In order to make sure that any point on the breadboard is really 0 V,  you must physically connect it with a wire (or a resistor) to the ground rail. This is an issue if you wish to test each element as you assemble the system. For example, consider the follower/voltage-divider/differential-amplifier circuit of the main text’s Figure 2B.  In the picture below, the blue wire (connected to row 16 of the breadboard’s upper half) comes from the amplifier’s scaled Vm output; it carries the signal we call VIN in the manuscript and on the Calibration menu page.

![lazy.jpg](/dynamicclampsitetwo/assets/img/Picture8.png "lazy.png")

If we were to disconnect the blue wire from the board, row 16 (which is also connected to op-amp pin 3) would NOT be at 0 V. Its voltage would be indeterminate; on a multimeter, it would appear to be floating randomly. In order to check the output of the circuit when VIN is equal to zero, we cannot just disconnect the blue wire — we must also physically connect row 16 with the ground rail (the blue rail at the very top). Then — and only then — can we say that VIN is equal to zero.

After the system has been assembled, this will only be an issue if you leave the inputs VIN (main text Fig. 2B, from the amplifier, representing the membrane potential Vm) or VDAQ (main text Fig. 2E, from the DAQ board, representing the current clamp system’s current injection command) unconnected. People are unlikely to do this in practice, but one way of accounting for the possibility is to connect both points to the ground rail with a “pull down resistor” (https://playground.arduino.cc/CommonTopics/PullUpDownResistor). These are more commonly used in digital circuits, but they can serve here too. For the case of VIN, connect one of the empty holes in upper row 16 with a resistor of very high resistance (say, 1 MOhm). When the blue wire is connected to the amplifier’s scaled output (and the amplifier is powered up), this 1 MOhm resistor will have negligible effect (it draws very little current because it is so big and won’t distort the circuit in a noticeable way). But when the blue wire is disconnected, the 1 MOhm resistor serves to connect row 16 (and hence, op-amp pin 3) physically to ground; that is, it makes the disconnected state equivalent to VIN = 0 V. Likewise, you might connect one of the breadboard holes in the same row as VDAQ to the upper blue ground rail with a 1 MOhm resistor.

![lazy.jpg](/dynamicclampsitetwo/assets/img/Picture16.png "lazy.png")

In the above picture, this means put one end of the 1 MOhm resistor into upper row 32 (same row as the rightmost wire of resistor R12) and the other end into ground rail at the top of the picture.

***

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>


 [1]: {{ site.url }}/blog/
 [2]: {{ site.url }}/blog/archive/
 [3]: http://foundation.zurb.com/docs/components/accordion.html
 [4]: {{ site.url }}/design/gallery/
 [5]: {{ site.url }}/design/video/
 [6]: https://www.google.de/maps/place/Strandpaviljoen+Joep+B.V./@51.9960733,5.830135,6z/data=!4m2!3m1!1s0x47cf5918df69093b:0x7c11ab31102c1c8a
 [7]: fontcustom.com
 [8]: https://www.tawk.to
 [9]: https://github.com/jjamor
 [10]: #
