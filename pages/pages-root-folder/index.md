---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: page-fullwidth
#header:
#  image_fullwidth: header_unsplash_12.jpg


# Necessary for index to generate, probably because homepage: true below...?
widget1:
  title: "Blog & Portfolio"
  url: 'http://phlow.github.io/feeling-responsive/blog/'
  image: widget-1-302x182.jpg
  text: '.'

#
# Use the call for action to show a button on the frontpage
#
# To make internal links, just use a permalink like this
# url: /getting-started/
#
# To style the button in different colors, use no value
# to use the main color or success, alert or secondary.
# To change colors see sass/_01_settings_colors.scss
#

# Necessary for index to generate, probably because homepage: true below...?
callforaction:
  url: https://tinyletter.com/feeling-responsive
  text: Inform me about new updates and features ›
  style: alert
permalink: /index.html

#
# This is a nasty hack to make the navigation highlight
# this page as active in the topbar navigation
#
homepage: true
---

# Introduction

<span style='color:red;'>The dynamic clamp paper may be downloaded from the eNeuro website </span>[http://www.eneuro.org/content/4/5/ENEURO.0250-17.2017](http://www.eneuro.org/content/4/5/ENEURO.0250-17.2017).

<span style='color:red;'>The software may be downloaded from the Github website </span>[http://github.com/nsdesai/dynamic_clamp](http://github.com/nsdesai/dynamic_clamp).

The dynamic clamp should be a standard part of every cellular electrophysiologist’s toolbox. That it is not, even 25 years after its introduction, comes down to three issues: money, the disruption that adding dynamic clamp to an existing electrophysiology rig entails, and the technical prowess required of experimenters. These have been valid and limiting issues in the past, but no longer. Technological advances associated with the so-called “maker movement” render them moot. We demonstrate this by implementing a fast (~100 kHz) dynamic clamp system using an inexpensive microcontroller (Teensy 3.6). The overall cost of the system is less than USD$100, and assembling it requires no prior electronics experience.  Modifying it – for example, to add Hodgkin-Huxley-style conductances – requires no prior programming experience. The system works together with existing electrophysiology data acquisition systems (for Macintosh, Windows, and Linux); it does not attempt to supplant them. Moreover, the process of assembling, modifying, and using the system constitutes a useful pedagogical exercise for students and researchers with no background but an interest in electronics and programming. We demonstrate the system’s utility by implementing conductances as fast as a transient sodium conductance and as complex as the Ornstein-Uhlenbeck conductances of the “point conductance” model of synaptic background activity.

***

<div style="text-align: center;">
    <span style='color:#29726F'>
    Updates<br> <br>

        Earlier updates have moved to the Updates tab. Relevant newer <span style='color:red'>updates</span> are shown here for convince
    </span>
</div>

***

<span style='color:#29726F;'>(July 2021):</span> Aditya Asopa [Github](https://github.com/AdityaAsopa) has written a detailed manual for setup and calibration of his printed circuit board (PCB) version of the dynamic clamp system (the 2019 version). He carefully explains and walks the reader through testing each stage of the system (power supply, voltage regulator, input circuit, and output circuit). The manual can be found on this project’s Github site: [PCB_manual](https://github.com/nsdesai/dynamic_clamp/tree/master/Setup%20and%20Calibration%20Manual%20for%20ver2.0).

Here is a picture of the completed PCB board. It has been tested and looks great.

![lazy.jpg](/dynamicclampsitetwo/assets/img/Picture1-1024x512.png "lazy.png")

<span style='color:#29726F;'>(September 2020):</span> Aditya Asopa [Github](https://github.com/AdityaAsopa) has designed a printed circuit board (PCB) version that can be used in place of the breadboard. It includes some nice additional features, including a toggle switch to bypass the dynamic clamp circuitry (useful if one wants to work just in current clamp). A preliminary design (not quite finalized) is available for inspection and download from this project’s Github site (dynamic_clamp). A final version will soon be available, as will a fuller description of Aditya’s contribution, which will be posted here.

<span style='color:#29726F;'>(October 2019):</span>  Kyle Wedgwood [Github](https://github.com/kyle-wedgwood) has written a Matlab controller to replace the original Processing sketch. It includes a flexible means of adding conductances to be controlled by the program. It is available at his Github site: [DynamicClampController](https://web.archive.org/web/20220123075404/https://github.com/kyle-wedgwood/DynamicClampController).

<span style='color:#29726F;'>(October 2019):</span> I tried quantifying the accuracy and noise of the redesigned dynamic clamp circuits (see September 2019 update and the CircuitLab tab). Here is a document describing what I found ([Accuracy_and_noise](https://1drv.ms/b/s!AiPVSLtiwPKMg482mAtfJKYtfr4qHA?e=n3QN2K)) . TL/DR: The new designs may eliminate the need for calibrating the system and generally make the system more accurate and less noisy.

<span style='color:#29726F;'>(September 2019):</span> I finally started writing a description of the new and improved circuit designs that I mentioned in my July 2019 update and that are shown in the CircuitLab section of this site. Here is a link to a draft version of a new methods section:  [Draft_of_revised_methods](https://1drv.ms/b/s!AiPVSLtiwPKMg48q1kXXKjrQ2-UtIQ?e=q008aB).  Once I finish editing that, I will revise the construction section to reflect the new designs. Any comments on or suggestions for the draft methods section would be much appreciated. Here is a picture of the new prototype:

![lazy.jpg](/dynamicclampsitetwo/assets/img/20190929_174853-1-1024x538.jpg "lazy.png")

<span style='color:#29726F;'>Python Alternative:</span>   Christian Rickert (GitHub) has developed software that expands  and improves the dynamic clamp project in two distinct ways: (1) an alternative Arduino sketch that provides for robust serial communication between microcontroller and host computer (dyClamp) and (2) a Python interface to control simulations (pyClamp). These are described in the DYCLAMP & PYCLAMP section of this site and are available at his Github site (linked above).

<span style='color:#29726F;'>Related Website:</span> Andrew Scallon at the University of Colorado has refined the design to make the dynamic clamp system more compact and rugged. Here is his website: [ONEdynamicclamp.](https://optogeneticsandneuralengineeringcore.gitlab.io/ONECoreSite/projects/DynamicClamp/)

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>
