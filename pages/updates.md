---
layout: page-fullwidth
title: "Updates"
#meta_title: "Feeling Responsive Theme Changelog"
#subheadline: "Feeling Responsive Theme Changelog"
#teaser: "History and changelog of Feeling Responsive Theme"
#header:
#   image_fullwidth: "header_unsplash_9.jpg"
permalink: "/updates/"
---

<span style='color:color:#29726F;'>(July 2021):</span> Aditya Asopa ([Github](https://github.com/AdityaAsopa)) has written a detailed manual for setup and calibration of his printed circuit board (PCB) version of the dynamic clamp system (the 2019 version). He carefully explains and walks the reader through testing each stage of the system (power supply, voltage regulator, input circuit, and output circuit). The manual can be found on this project’s Github site: PCB_manual.

Here is a picture of the completed PCB board. It has been tested and looks great.

![lazy.jpg](/dynamicclampsitetwo/assets/img/Picture1-1024x512.png "lazy.png")

<span style='color:color:#29726F;'>(September 2020):</span> Aditya Asopa ([Github](https://github.com/AdityaAsopa)) has designed a printed circuit board (PCB) version that can be used in place of the breadboard. It includes some nice additional features, including a toggle switch to bypass the dynamic clamp circuitry (useful if one wants to work just in current clamp). A preliminary design (not quite finalized) is available for inspection and download from this project’s Github site (dynamic_clamp). A final version will soon be available, as will a fuller description of Aditya’s contribution, which will be posted here.

<span style='color:color:#29726F;'>(October 2019):</span>  Kyle Wedgwood ([Github](https://github.com/kyle-wedgwood)) has written a Matlab controller to replace the original Processing sketch. It includes a flexible means of adding conductances to be controlled by the program. It is available at his Github site: [DynamicClampController](https://github.com/kyle-wedgwood/DynamicClampController).

<span style='color:color:#29726F;'>(October 2019):</span> I tried quantifying the accuracy and noise of the redesigned dynamic clamp circuits (see September 2019 update and the CircuitLab tab). Here is a document describing what I found ([Accuracy_and_noise](https://1drv.ms/b/s!AiPVSLtiwPKMg482mAtfJKYtfr4qHA?e=n3QN2K)). TL/DR: The new designs may eliminate the need for calibrating the system and generally make the system more accurate and less noisy.

<span style='color:color:#29726F;'>(September 2019):</span> I finally started writing a description of the new and improved circuit designs that I mentioned in my July 2019 update and that are shown in the CircuitLab section of this site. Here is a link to a draft version of a new methods section:  [Draft_of_revised_methods](https://1drv.ms/b/s!AiPVSLtiwPKMg48q1kXXKjrQ2-UtIQ?e=q008aB).  Once I finish editing that, I will revise the construction section to reflect the new designs. Any comments on or suggestions for the draft methods section would be much appreciated. Here is a picture of the new prototype:

![lazy.jpg](/dynamicclampsitetwo/assets/img/20190929_174853-1-1024x538.jpg "lazy.png")

<span style='color:color:#29726F;'>Python Alternative:</span>   Christian Rickert ([GitHub](https://github.com/christianrickert)) has developed software that expands  and improves the dynamic clamp project in two distinct ways: (1) an alternative Arduino sketch that provides for robust serial communication between microcontroller and host computer (dyClamp) and (2) a Python interface to control simulations (pyClamp). These are described in the DYCLAMP & PYCLAMP section of this site and are available at his Github site (linked above).

<span style='color:color:#29726F;'>Related Website:</span> Andrew Scallon at the University of Colorado has refined the design to make the dynamic clamp system more compact and rugged. Here is his website: [ONEdynamicclamp](https://optogeneticsandneuralengineeringcore.gitlab.io/ONECoreSite/projects/DynamicClamp/)

<span style='color:color:#29726F;'>(January 2018):</span> Added a Media section to hold videos and pictures.

Refined the description of the calibration procedure and edited the associated Processing sketch (processing_calibration.pde); now measurements will be plotted as they’re being obtained.

<span style='color:color:#29726F;'>(December 2017):</span> Added a section (External or saved conductances) that describes how to send the Teensy microcontroller analog signals representing up to three separate conductance trains, so that the Teensy can inject these conductances into a neuron. A typical use case would be when an experimenter, having previously recorded excitatory and inhibitory synaptic currents in voltage clamp mode (perhaps in vivo or in culture), would like to feed conductances derived from these recordings into a different neuron (perhaps in a brain slice).

The added section also describes how to save conductance waveforms in the Teensy’s memory so that they can be read out element by element at run-time, rather than being generated by numerical integration.

Also added a section (Troubleshooting) which will include troubleshooting tips.

***

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>


 [1]: {{ site.url }}/blog/
 [2]: {{ site.url }}/blog/archive/
 [3]: http://foundation.zurb.com/docs/components/accordion.html
 [4]: {{ site.url }}/design/gallery/
 [5]: {{ site.url }}/design/video/
 [6]: https://www.google.de/maps/place/Strandpaviljoen+Joep+B.V./@51.9960733,5.830135,6z/data=!4m2!3m1!1s0x47cf5918df69093b:0x7c11ab31102c1c8a
 [7]: fontcustom.com
 [8]: https://www.tawk.to
 [9]: https://github.com/jjamor
 [10]: #
